use eigen; #use database
select count(*) from first; #count the total number
delete from first where age < 0; 


select * from first #list all the duplicate data
  where id in 
  ( select id from first group by id having count(id) > 1);
create table second as #create another table with distinct id
  select distinct id, name,  email, age from first;
select count(*) from second;
select * from second #check whether id is distinct
  where id in 
  ( select id from second group by id having count(id) > 1);
  
  
delete from second where name is null or email is null;
delete from second where name = '' or email = '';
delete from second where email not like'%@example.com';
delete from second where length(name) <> 10 
  or length(email) <> 22;
delete from second 
  where email <> concat_ws('@', name, 'example.com');
  

select count(*) from second;#count the total number of correct data